#!/bin/bash
DIR=$(dirname $0)

USER="$(whoami)"
[ -z "$USER" ] && echo "Unable to Fetch Username" && exit 1

ANSIBLE_HOST_KEY_CHECKING=False $HOME/.local/bin/ansible-playbook -i 127.0.0.1, -K --extra-vars "username=$USER" $DIR/bootstrap.yml
