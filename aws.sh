#!/bin/bash
# Customisation for AWS EX2 Instances
DIR=$(dirname $0)
source .env
USER="$(whoami)"

[ -z "$USER" ] && echo "Unable to Fetch Username" && exit 1
[ -z "$ALIAS" ] && echo "ALIAS not set" && exit 1
[ -z "$EMAIL" ] && echo "EMAIL not set" && exit 1

# Install Required Roles
ansible-galaxy install -r requirements.yml
[ $? != 0 ] && echo "Error Installing Dependencies" && exit 1
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i 127.0.0.1, --extra-vars "username=$USER alias=$ALIAS email=$EMAIL $DIR/aws.yml
