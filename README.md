# README

**Provisioning for vanilla *Ubuntu* Machines on virtualbox, AWS or Native**

Profile configuration for VM settings

Setup user profiles correctly, install core software and get your started

* init.sh: Minimal bootstrap script to get core ansible dependancies installed - Needs running only ONCE
* bootstrap.sh: Wrapper around Ansible-Playbook bootstrap.yml
* bootstrap.yml: Bootstrap with Ansible - Needs running only ONCE
* playbook.sh: Wrapper around Ansible-Playbook playbook.yml
* playbook.yml: Ansible Magic happens here
* virtualbox.sh: Wrapper around Ansible-Playbook playbook.yml
* virtualbox.yml: Ansible Magic happens here

This repo will:

- Initialise your system and Install Core Packages
- Setup Base User Access
- Install requried software and configurations

## Setup

First download this repo to your local machine, via HTTP as an archive.

Initialise and Bootstrpap the VM:

Invoke the `init.sh` script to install basic packages and setup the environment:

`./init.sh`

When it asks for `BECOME Password` enter YOUR VM password

After this completes, logout and login to reset your session. *This reloads your user groups.*

Create the Vault Password, for the secret credentials, substitute `<passphrase>` to the correct value: `echo "<passphrase>" > .vaultpass`

**Set your User Information, used for setting up git from `.env.template`**

* Create your `.env` from the template: `cp .env.template .env`
* Edit the `.env` to include the relevant settings
---
Install remaining software with `playbook.sh`:
`./playbook.sh`

After this completes, logout and login to reset your session. This reloads your user groups to allow you to use docker
If the configurations change or new software is added, you only need to run `playbook.sh` to update your configuration


## ToDo List

* [x] Split playbook into seperate playbooks of initial and update
* [x] Instructions
* [ ] VM Build Instructions
* [ ] Test
* [ ] AWS CLI https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html


