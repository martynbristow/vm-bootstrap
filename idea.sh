#!/bin/bash
DIR=$(dirname $0)
ALIAS="TRT-MartynB"
EMAIL="martyn.bristow@bt.com"
IJ_VERSION="2020.1"
IJ_EDITION="community"
USER="$(whoami)"
[ -z "$USER" ] && echo "Unable to Fetch Username" && exit 1
[ -z "$ALIAS" ] && echo "ALIAS not set" && exit 1
[ -z "$EMAIL" ] && echo "EMAIL not set" && exit 1

# Install Required Roles
ansible-galaxy install -r requirements.yml
[ $? != 0 ] && echo "Error Installing Dependancies" && exit 1
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i 127.0.0.1, --extra-vars "username=$USER alias=$ALIAS email=$EMAIL ij_version=$IJ_VERSION ij_edition=$IJ_EDITION" --vault-password-file .vaultpass $DIR/idea.yml
