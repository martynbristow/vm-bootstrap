#!/bin/bash
# Bootstrap an Ubuntu Box
DIR=$(dirname $0)
printf "This will install some basic software that allows you to provision it for to use this VM as a development environment\n"

sudo apt update
sudo apt -y upgrade
pip install ansible 

ssh-keygen -q
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

