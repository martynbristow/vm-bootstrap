#!/bin/bash
# Bootstrap an Ubuntu Box
DIR=$(dirname $0)
printf "This will install some basic software that allows you to provision it for to use this VM as a development environment\n"

sudo apt update
sudo apt -y upgrade

sudo apt install -y python3 vim python3-distutils openssh-server sshpass
wget https://bootstrap.pypa.io/get-pip.py -O /tmp/get-pip.py
python3 /tmp/get-pip.py
$HOME/.local/bin/pip install ansible
printf 'Generating SSH Key ... follow the prompts'
ssh-keygen -q
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

echo "Installed Pip and Ansible, you need to logout and login to complete the process"
